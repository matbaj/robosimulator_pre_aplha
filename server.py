#!/usr/bin/python
import math
import time   
from socket import *

from PIL import Image
from scipy import spatial
from daemon import runner
import threading,os,sys,select
import socket
import code
import cmd
import pygame
from time import sleep



class View:
  def __init__(self,caption,map_file, bimo_x = 10, bimo_y = 10, bimo_angle = 0):
    bimo = pygame.image.load('img/bimo.png')
    red_dot = pygame.image.load('img/red_dot.png')
    self.objects = {'bimo': bimo, 'red_dot':red_dot}
    self.background = pygame.image.load(map_file)
    self.backgroundRect = self.background.get_rect()
    size = (width, height) = self.background.get_size()
    self.screen = pygame.display.set_mode(size)
    pygame.display.set_caption(caption)
    self.elements = []
    self.bimo_x = bimo_x
    self.bimo_y = bimo_y
    self.bimo_angle = bimo_angle
    self.draw()

  def draw(self):
    self.screen.blit(self.background, self.backgroundRect)
    bimo = pygame.transform.rotate(self.objects['bimo'], -self.bimo_angle)
    (bimo_width, bimo_height) = bimo.get_size()
    self.screen.blit(bimo, (self.bimo_x-(bimo_width/2),self.bimo_y-(bimo_height/2)))
    for element in self.elements:
      (x,y,object_name) = element
      self.screen.blit(self.objects[object_name], (x,y))
    pygame.display.flip() 

  def __del__(self):
    pygame.quit()

class ImageReader(object):
   
   def __init__(self, filename):
       self.width  = 0
       self.height = 0
       self.photo = Image.open(filename)
       self.image = self.photo.load()
       self.width, self.height = self.get_max()

       
   def getList(self):
   
       result = []    
       i = 0
       print self.width
       print self.height
       while i < self.width:
           j = 0
           while j < self.height:
               if self.image[i, j] <> (255, 255, 255):
                  result.append( (i,j) )
                  
               j +=1
               
           i += 1
           
       return result

   def get_max(self):
       return self.photo.size

class ServerTCP(threading.Thread): 
  def __init__(self, port=8888): 
    threading.Thread.__init__(self)
    self._stop = threading.Event()
    self.host = ''
    self.pipe = 0
    self.port = port 
    self.backlog = 5 
    self.size = 10240 
    self.server = None 
    self.threads = [] 
    self.pipe = os.pipe()

    
  def open_socket(self): 
      try: 
          self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
          self.server.bind((self.host,self.port)) 
          self.server.listen(5) 
      except socket.error, (value,message): 
          if self.server: 
              self.server.close() 
          print "Could not open socket: " + message 
          sys.exit(1) 
  
  def run(self): 
    self.open_socket() 
    input = [self.server,self.pipe[0]] 
    self.running = 1 
    while self.running: 
      try:
        inputready,outputready,exceptready = select.select(input,[],[]) 
      except:
        print 'Something is wrong'
        print sys.exc_info()
        inputready = ''
        self.running = 0

      for s in inputready: 
        if s == self.server: 
          # handle the server socket 
          c = Client(self.server.accept()) 
          c.start() 
          self.threads.append(c)
        elif s == self.pipe[0]: 
          try:
            cmd  = os.read(self.pipe[0], 64).split(":")
          except:
            cmd = ['servertcp', 'quit']
          if cmd[0] == 'servertcp':
            if cmd[1] =='quit':
              print 'ServerTCP: shutting down'
              self.running = 0

    #disconnect all clients
    for node in self.threads:
      node.quit()
    self.server.close() 
    # close all threads 
    for c in self.threads: 
      c.join() 
 
class Client(threading.Thread): 
  def __init__(self,(client,address)): 
    threading.Thread.__init__(self) 
    self.client = client 
    self.address = address 
    self.size = 1024 
    self.pipe = os.pipe()
    self.stdin_path = '/dev/null'
    self.stdout_path = '/dev/tty'
    self.stderr_path = '/dev/tty'
    self.pidfile_path =  '/tmp/roboserver.pid'
    self.pidfile_timeout = 5
    self.angle = 0
    self.y = 300.0
    self.x = 100.0
    self.max_y = 0
    self.max_x = 0
    self.range = 200
    self.tree = None

  def loadPhoto(self, filename):
    image = ImageReader(filename)
    self.pixels = image.getList()
    self.tree = spatial.KDTree(self.pixels)
    self.max_x, self.max_y = image.get_max()
    self.view = View("Robot Simulator @%s:%s" % self.address,filename, self.x,self.y, self.angle)
    self.view.draw()
    return 'OK'
       
  def rotation(self, angle):
    self.angle = ( self.angle + angle ) % 360
    self.view.bimo_angle = self.angle
    self.view.draw()
    return str(self.angle)
          
  def forward(self, move):
    rad = math.radians(self.angle)
    y = self.y + math.sin(rad) * move
    x = self.x + math.cos(rad) * move
    if not (0 <= y <= self.max_y) or not( 0 <= x <= self.max_x):
      return "ruch niemozliwy"
    else:
      self.x = x
      self.y = y 
      self.view.bimo_x = x
      self.view.bimo_y = y
      self.view.draw()
    return str( (self.x, self.y) )
           
  def set_range(self, r):
    self.range = r
    return str( self.range )
          
  def look(self):
    def convert(pair):
      a = self.x - pair[0]
      b = self.y - pair[1]
      r = int(math.sqrt(a*a + b*b))

      fi = int(math.degrees( math.atan(b/a) )) if a != 0 else 90
      fi = fi+180 if (a > 0) else fi
      fi = (fi-self.angle)%360
      return fi, r

    result = []
    indexes = self.tree.query_ball_point((self.x,self.y),self.range)
    elements = []


    for i in indexes:
      result.append(self.pixels[i])
    
    # for pair in result:


    points = map(convert, result)
    points.sort()
    last_fi = -400

    inrange = []
    elements = []
    for fi, r in points:
      if last_fi == fi:
        continue
      last_fi = fi
      rad = math.radians((fi+self.angle)%360)
      y = self.y + math.sin(rad) * r
      x = self.x + math.cos(rad) * r
      inrange.append("%s:%s" %(fi, r))
      elements.append((x,y, 'red_dot'))
      self.view.elements = elements
      self.view.draw()
      sleep(0.01)

    return "|".join(inrange)+"|"

  def quit(self):
    os.write(self.pipe[1], 'node:quit')

  def run(self):
    print "New Client", self.address  
    running = 1
    self.authorized = False 
    self.count_try = 0
    input = [self.client,self.pipe[0]]
    while running:
      try:
        inputready,outputready,exceptready = select.select(input,[],[]) 
      except:
        print 'Something is wrong'
        running = 0
      for s in inputready: 
        if s == self.client:
            msg = s.recv(self.size)
            if msg:
              print "MSG: %s" % msg
              try:
                state, val = msg.split(':')
              except:
                state = ''

              response = ''

              if state == 'FORWARD':
                response = self.forward( int(val) )
              if state == 'BACKWARD':
                response = self.forward( -1*int(val) )
              elif state == 'ROTATION':
                response = self.rotation( int(val) )
              elif state == 'RANGE':
                response = self.set_range( int(val) )
              elif state == 'LOOK':
                response = self.look()
              elif state == 'LOAD':
                response = self.loadPhoto(val)
              else:
                response = 'ERROR' 

              s.send(response)
            else:
              running = 0
        elif s == self.pipe[0]: 
          try:
            cmd  = os.read(self.pipe[0], 64).split(":")
          except:
            cmd = ['node', 'quit']
          if cmd[0] == 'node':
            if cmd[1] =='quit':
              running = 0 
    print "Closing connection with ", self.address       
    self.client.close()  
    del self.view

class Server(cmd.Cmd):
  prompt = "(Server)>"

  buf_size = 10240

  def do_inter(self, line):
    """Droping to interpreter"""
    code.interact(local=locals())

  def do_client_list(self,line):
    """client_list - list of connected clients"""
    for client in server.threads:
      print client.address 

  def do_quit(self, line):
    """Quit from client"""
    os.write(server.pipe[1], 'servertcp:quit')
    return True

  def do_EOF(self, line):
    """Quit from client"""
    os.write(server.pipe[1], 'servertcp:quit')
    return True

if __name__ == '__main__':
    print "RoboSimulator Server"
    cli = Server()
    server = ServerTCP()

    server.start()
    try:
        cli.cmdloop()
    except KeyboardInterrupt:
        cli.do_quit('')
