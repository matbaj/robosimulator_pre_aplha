##### INSTALACJA

w pliku req.txt znajdują się zależności pythonowe.
instalacja: pip install -r req.txt

Być może instalacja się nie powiedzie z braku jakichś pakietów w systemie. Wówczas czytamy komunikat i instalujemy.

Na pewno będzie oczekiwał kompilatora Fortrana.


#### użytkowanie

python server.py run

System zacznie pracować jako deamon i nasłuchiwać na localhost:8888.

Możemy mu przesyłać komunikaty postaci <POLECENIE>:<WARTOSC>

Dostępne polecenia:

FORWARD - przesuwa robota o WARTOSC do przodu
ROTATION - obraca robota o WARTOSC stopni
RANGE - zmienia zasięg widzenia (domyślnie 10 pikseli) do WARTOSC
LOOK - WARTOSC jest wyjatkowo pusta (ale ':' jest nadal konieczny). podaje punktu widoczne z aktualnego położenia postaci [(fi_1, r_1), (fi_2, r_2), ..., (fi_n, r_n)] fi - kąt, r - odległość
LOAD - ładuje plik WARTOSC (wskazany png) wszystko co nie jest białe jest 'ścianą'.

w client.py znajduje się prymitywna konsola.


