import socket
import code
import cmd


class Client(cmd.Cmd):
  prompt = "(not connected)>"
  connected = False
  buf_size = 10240
  def do_connect(self,line):
    """connect $host $port - connect to server"""
    if self.connected:
      print "Already connected"
    else:
      try:
        host , port =  line.split(" ")
      except:
        print 'Wrong args. Setting default'
        host = 'localhost'
        port = 8888
      self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      print "Connecting to: %s:%s" % (host,port)
      self.socket.connect((host, port))
      self.prompt = "%s:%s>" % (host,port)
      self.connected = True

  def do_disconnect(self,line):
    """disconnect - disconnect from server"""
    if self.connected:
      print "Disconnecting"
      self.socket.close()
      self.connected = False
      self.prompt = "(not connected)>" 
    else:
      print 'Not connected'

  def do_load(self,line):
    """load $file_path - load map in simulator"""
    if self.connected:
      filename = line
      self.socket.send('LOAD:'+filename)
      print self.socket.recv(self.buf_size)
    else:
      print 'Not connected'

  def do_rotate(self,line):
    """rotate $angle - rotate robot in specific angle"""
    if self.connected:
      angle = line
      self.socket.send('ROTATION:'+angle)
      print self.socket.recv(self.buf_size)
    else:
      print 'Not connected'

  def do_look(self,line):
    """look  - retrieve array with measurements"""
    if self.connected:
      self.socket.send('LOOK:')
      print self.socket.recv(self.buf_size)
    else:
      print 'Not connected'

  def do_range(self,line):
    """range $val - sets range in simulator"""
    if self.connected:
      range_val = line
      self.socket.send('RANGE:'+range_val)
      print self.socket.recv(self.buf_size)
    else:
      print 'Not connected'

  def do_forward(self,line):
    """forward $val"""
    if self.connected:
      steps = line
      self.socket.send('FORWARD:'+steps)
      print self.socket.recv(self.buf_size)
    else:
      print 'Not connected'

  def do_backward(self,line):
    """backward $val"""
    if self.connected:
      steps = line
      self.socket.send('BACKWARD:'+steps)
      print self.socket.recv(self.buf_size)
    else:
      print 'Not connected'
      
  def do_inter(self, line):
    """Droping to interpreter"""
    code.interact(local=locals())

  def do_quit(self, line):
    """Quit from client"""
    if self.connected:
      self.do_disconnect('')
    return True

  def do_EOF(self, line):
    """Quit from client"""
    if self.connected:
      self.do_disconnect('')
    return True



if __name__ == '__main__':
    print "RoboSimulator Client"
    cli = Client()
    try:
        cli.cmdloop()
    except KeyboardInterrupt:
        cli.do_quit('')



