PIL==1.1.7
argparse==1.2.1
lockfile==0.9.1
numpy==1.6.2
python-daemon==1.6
scipy==0.11.0
wsgiref==0.1.2
